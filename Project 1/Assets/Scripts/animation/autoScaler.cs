﻿using UnityEngine;
using System.Collections;

public class autoScaler : MonoBehaviour
{
	//scale amounts are additional scaling not multlicative scaling.
	public float XscaleAmount;
	public float YscaleAmount;
	public float ZscaleAmount;
	//cycle.(1 = 1 loop/sec)
	public float scaleRate;

	//should scaling also decrease scale when looping or only return to model base scale?
	public bool minIsBaseScale;

	private float i;
	private float scalage;
	private Vector3 baseScale;

	void Start() {
		i = 0.0f;
		baseScale = this.transform.localScale; // grab original baseScale for later reference.
	}

	void Update ()
	{
		scalage = Mathf.Sin (i); 
		if (minIsBaseScale) { //limit bottom of sin loop if returning only to original model scal.
			scalage = (scalage + 1) / 2;
		}
		this.i += scaleRate*Time.deltaTime;  //update scaling amount.
		i = i%360; //prevents errors after long runtime.
		this.transform.localScale = baseScale + new Vector3(scalage*XscaleAmount, scalage*YscaleAmount, scalage*ZscaleAmount);//do the scaling.
	}
}