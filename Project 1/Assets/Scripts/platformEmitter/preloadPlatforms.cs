﻿/*
 this is used to preload the platforms before a level starts.
 good base values i have found are 100 scale and around 80 or so waiting seconds.

this should later be imporved to also cover the screen and prevent user input while the level is populating.

this should not be attached to platform emitters, rather a single one of these scripts shuold be attached to the level.
or possibly the player.

after the fastforward period of time this script deletes itself.
*/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class preloadPlatforms : MonoBehaviour {

	public float scale;
	public float time;

    private float oldDelta;
    private float oldFixed;

	// Use this for initialization
	void Start () {

		Time.timeScale = scale;

        oldDelta = Time.maximumDeltaTime;
        Time.maximumDeltaTime = 10.0f;

        oldFixed = Time.fixedDeltaTime;
        Time.fixedDeltaTime = 0.5f;

	}
	
	// Update is called once per frame
	void Update () {

		time -= Time.deltaTime;
		if (time < 0) {
			Time.timeScale = 1;
            Time.maximumDeltaTime = oldDelta;
            Time.fixedDeltaTime = oldFixed;
			Destroy (this);
		}
	}
}
