﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class emittedPlatform : MonoBehaviour {

	public float speed;

	public float lifeTime;

	private Rigidbody self;


	// Use this for initialization
	void Start () {
		while (self == null) {
			self = this.GetComponent<Rigidbody> ();
		}
        

        
	}

	// Update is called once per frame
	void FixedUpdate () {


        self.MovePosition(this.transform.position + (self.rotation * (Vector3.forward * speed * Time.fixedDeltaTime)));

        lifeTime -= Time.deltaTime;
        if (lifeTime < 0)
        {
            Object.Destroy(gameObject);
        }
    }

    
}
