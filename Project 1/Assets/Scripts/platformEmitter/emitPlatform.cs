﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class emitPlatform : MonoBehaviour {
	public float emitPeriod;
	public emittedPlatform platform; //must have a rigidbody.
	public float speed;
	public float emittedLifeTime;

	private float emitTimer;

    private bool shouldEmitAPlat;

	// Use this for initialization
	void Start () {
        shouldEmitAPlat = true;
        StartCoroutine(timer(emitPeriod));
		//emitTimer = emitPeriod;
	}
	
	// Update is called once per frame
	void Update () {
        /*
		emitTimer -= Time.deltaTime;
		if (emitTimer < 0) {
			emitTimer = emitPeriod;
			emit ();
		}
        */

        if (shouldEmitAPlat)
        {
            emit();
            shouldEmitAPlat = false;
        }
	}

	private void emit(){
		emittedPlatform newPlatform = Instantiate (platform,this.transform.position,this.transform.rotation);

		newPlatform.speed = this.speed;
		newPlatform.lifeTime = emittedLifeTime;
	}

    private void startTimerLoop()
    {
        StartCoroutine(timer(emitPeriod));
    }

    IEnumerator timer(float p)
    {
        yield return new WaitForSeconds(p);
        shouldEmitAPlat = true;
        startTimerLoop();
    }
}
