﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goalScript : MonoBehaviour {
    
    public AudioClip soundSuccess;
    private AudioSource audioSource;

    public GameObject trophy;
    private GameObject actualTrophy;

    public GameObject lightPrefab;
    private GameObject light;
    

    public float growthRate;
    public float growthTime;

    private bool triggered;
    private bool firstTrigger;

    private Vector3 initialScale;


    // Use this for initialization
    void Start () {

        audioSource = this.GetComponent<AudioSource>();
        triggered = false;
        firstTrigger = true;

        initialScale = trophy.transform.localScale;

        actualTrophy = Instantiate(trophy);

        actualTrophy.transform.position = this.transform.position - new Vector3(0,0.2f,0);
	}
	
	// Update is called once per frame
	void Update () {
        if (triggered && growthTime > 0)
        {

            actualTrophy.transform.localScale += initialScale * growthRate * Time.deltaTime;
            growthTime -= Time.deltaTime;

        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && firstTrigger == true)
        {
            audioSource.PlayOneShot(soundSuccess);
            triggered = true;
            firstTrigger = false;


            light = Instantiate(lightPrefab,this.transform);
            light.transform.position = this.transform.position + new Vector3(0,3.8f,0);
            light.transform.rotation = Quaternion.Euler(90, 0, 0);
        }
       
    }
}
