﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public float speed = 15.0f;
    public float jumpSpeed = 5.0f;
    public float floatiness = 5.0f;

    public AudioClip soundJump;
    public AudioClip soundSplit;
    private AudioSource audioSource;

    private Rigidbody myRigidbody;

    private Vector3 movement;
    private bool isTouchingSomething;
    private bool canJump = true;
    private Vector3 jumpDir;
    private float bounceVel;

    //has to do with sound
    private float lastMagnitude;
    private bool didJump;

    public float airMovementRatio = 0.25f;

    public virtual bool jump
    {
        get
        {
            return Input.GetButton("Jump");
        }
    }
    public virtual float horizontal
    {
        get
        {
            return Input.GetAxis("Horizontal");
        }
    }
    public virtual float vertical
    {
        get
        {
            return Input.GetAxis("Vertical");
        }
    }

    void Start() {
        myRigidbody = GetComponent<Rigidbody>();
        myRigidbody.freezeRotation = true;

        audioSource = GetComponent<AudioSource>();
        lastMagnitude = 0;
        didJump = false;
    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        didJump = false;
        
        movement = new Vector3(horizontal, 0, vertical);
        movement = transform.TransformDirection(movement);
        movement = Vector3.ClampMagnitude(movement, 1.0f);
        movement *= speed * Time.deltaTime;

        Debug.DrawRay(transform.position, movement, Color.red, 10, false);

        if (isTouchingSomething) //ground/surface movement
        {
            transform.position += movement;
            //myRigidbody.MovePosition(myRigidbody.position + movement);
        }
        else //air movement
        {
            transform.position += movement * airMovementRatio;
            //myRigidbody.MovePosition(myRigidbody.position + movement * airMovementRatio);
        }

        //jumping
        if(jump)
        {
            myRigidbody.AddForce(Vector3.up * floatiness); //hold jump to slow fall

            if (isTouchingSomething && canJump)
            {
                //informs everything that the player did indeed trigger a jump this frame.
                didJump = true;

                //play sound whne jumping.
                audioSource.PlayOneShot(soundJump);

                //halts downward motion when you jump off something
                if (myRigidbody.velocity.y < 0)
                {
                    myRigidbody.velocity = flatten(myRigidbody.velocity);
                }

                //jumping
                myRigidbody.velocity += (jumpDir * jumpSpeed/2) + (Vector3.up * jumpSpeed/2);

                if (Vector3.Angle(Vector3.up, jumpDir) < 45)
                {
                    //myRigidbody.velocity += movement * (1.0f-airMovementRatio) * 1 / Time.deltaTime;     
                }
                else
                {
                    Vector3 bounce = myRigidbody.velocity.normalized * bounceVel;
                    if (myRigidbody.velocity.magnitude < bounce.magnitude)
                    {
                        myRigidbody.velocity = bounce;
                    }
                }

                canJump = false;
                StartCoroutine(JumpCooldown());
            }
        }

        #region split sound stuff
        float magnitude = myRigidbody.velocity.magnitude;
        if (!didJump)
        {
            if (Mathf.Abs(magnitude - lastMagnitude) > 5)
            {
                //play split sound effect.
                audioSource.PlayOneShot(soundSplit);
            }

        }
        lastMagnitude = magnitude;
        #endregion
    }

    void OnCollisionEnter(Collision collision)
    {
        isTouchingSomething = true;
        setJumpDir(collision);
        bounceVel = collision.relativeVelocity.magnitude;
    }

    void OnCollisionStay(Collision collision)
    {
        isTouchingSomething = true;
        setJumpDir(collision);
        bounceVel = 0.0f;
    }

    void OnCollisionExit(Collision collision)
    {
        isTouchingSomething = false;
        bounceVel = 0.0f;
        jumpDir = Vector3.zero;
        myRigidbody.velocity += movement * (1.0f - airMovementRatio) * 1 / Time.deltaTime;
    }

    void setJumpDir(Collision collision)
    {
        Vector3 sum = new Vector3();

        foreach (ContactPoint contact in collision.contacts)
        {
            sum += contact.normal;
        }

        jumpDir = sum.normalized;
    }

    Vector3 flatten(Vector3 v)
    {
        return new Vector3(v.x, 0, v.z);
    }

    IEnumerator JumpCooldown()
    {
        yield return new WaitForSeconds(0.1f);
        canJump = true;
    }
}
