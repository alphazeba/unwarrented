﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInput : MonoBehaviour {

    public float speed = 6.0f;
	public float jumpSpeed = 5;
	public float floatiness = 5;

	public AudioClip soundJump;
	private AudioSource audioSource;

    private Rigidbody myRigidbody;
	private JumpHandler jumpHandler;

    public virtual bool jump
    {
        get
        {
            return Input.GetButton("Jump");
        }
    }

    public virtual float horizontal
    {
        get
        {
            return Input.GetAxis("Horizontal");
        }
    }
    public virtual float vertical
    {
        get
        {
            return Input.GetAxis("Vertical");
        }
    }

    void Start () {
        myRigidbody = GetComponent<Rigidbody>();
        myRigidbody.freezeRotation = true;
		jumpHandler = new JumpHandler (this.GetComponent<SphereCollider> (),myRigidbody,jumpSpeed,floatiness);

		audioSource = GetComponent<AudioSource> ();
    }
	
	void FixedUpdate () {

		jumpHandler.update(jump);
		movement ();

		if (jumpHandler.didJump ()) {
			audioSource.PlayOneShot (soundJump);
		}

	}

	void movement(){
		float deltaX = horizontal * speed;
		float deltaZ = vertical * speed;
		Vector3 movement = new Vector3(deltaX, 0, deltaZ);

		movement = Vector3.ClampMagnitude(movement, speed);
		movement *= Time.deltaTime;
		movement = transform.TransformDirection(movement);
		transform.position += movement;
	}


	class JumpHandler {

		private bool canJump;
		private bool touchingGround;
		private float jumpTimer;
		//private SphereCollider jumpTrigger;
		private Rigidbody self;
		private float jumpSpeed;
		private float floatiness;

		private bool varDidJump;

		public JumpHandler(SphereCollider sc, Rigidbody rigidbody,float spd , float flt){
			//jumpTrigger = sc;
			//jumpTrigger.isTrigger = true;
			self = rigidbody;
			touchingGround = false;
			jumpSpeed = spd;
			floatiness = flt;

			varDidJump = false;
		}

		public void update(bool jump){

			//can jump if touching ground and hasn't jumped recently
			//otherwise, cannot jump.
			if (touchingGround && jumpTimer < 0) {
				canJump = true;
			} else {
				canJump = false;
			}

			varDidJump = false; //will be set to true if the player inputted a succesful jump.


			//if pressing jump and allowed to jump
			//will zero out y velocity if moving downwards so there is a minimum jump speed.//not sure if necessary.
			if (jump) {
				self.AddForce (Vector3.up * floatiness);
				if (canJump) {
					if (self.velocity.y < 0) {
						self.velocity = self.velocity - (self.velocity.y * Vector3.up); //zero out y velocity first.
					}
					self.velocity = self.velocity + (Vector3.up * jumpSpeed);  

					jumpTimer = 0.3f;
					varDidJump = true;
				}
			}

			//upate jumpTimer
			jumpTimer -= Time.deltaTime;
			//will be set to true on callback if touching ground.
			touchingGround = false;
		}

		public void setCanJump(bool j){
			canJump = j;
		}

		public bool didJump(){
			return varDidJump;
		}

		public void OnTriggerEnter(Collider other){
			touchingGround = true;
		}

		public void OnTriggerStay(Collider other){
			touchingGround = true;
		}
	}

	void OnTriggerEnter(Collider other){
		jumpHandler.OnTriggerEnter (other);
	}

	void OnTriggerStay(Collider other){
		jumpHandler.OnTriggerStay (other);
	}


}
	