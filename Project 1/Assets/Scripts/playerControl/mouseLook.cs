﻿/*
 * camera control that yaw's player but pitches the camera.
 * uses old input system currenlty.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseLook : MonoBehaviour {

	public float mouseXsensitivity = 10;
	public float mouseYsensitivity = 10;

	private float xRotation = 0;
	private float yRotation;

	private Camera mycamera;

	// Use this for initialization
	void Start () {
		mycamera = this.GetComponentInChildren<Camera> ();
        yRotation = this.transform.rotation.eulerAngles.y;
	}
	
	// Update is called once per frame
	void Update () {
		xRotation -= Input.GetAxis ("Mouse Y") * mouseYsensitivity;
		yRotation += Input.GetAxis ("Mouse X") * mouseXsensitivity;

		if (xRotation > 75) {
			xRotation = 75;
		}
		if (xRotation < -90) {
			xRotation = -90;
		}


		this.transform.rotation = Quaternion.Euler(0,yRotation,0);// = Quaternion.Euler (0, yRotation, 0);
		mycamera.transform.rotation = Quaternion.Euler(xRotation,yRotation,0);
	}
}
