﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fallDeath : MonoBehaviour {

	public float fallHeight;


	private Vector3 startPos;
	private bool dead;

	// Use this for initialization
	void Start () {
		startPos = this.transform.position;
		dead = false;
	}
	
	// Update is called once per frame
	void Update () {
		dead = false;
		if (this.transform.position.y < startPos.y - fallHeight) {
			//you die.
			dead = true;
			this.transform.position = startPos;
			this.GetComponent<Rigidbody> ().velocity = new Vector3 ();
		}
	}

	public bool hasDied(){
		return dead;
	}
}
