﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grapplingHook : MonoBehaviour {

    public Camera cam;
    public RaycastHit hit;
    public LayerMask cullingMask;
    public int maxDistance;
    public bool isGrappling;
    public Vector3 loc;
    public float speed = 10.0f;
    public Transform hand;
    public Rigidbody FPC;
    public LineRenderer LR;

	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Mouse0))
            findSpot();

        if (isGrappling)
        {
            Flying();
        }

        if(Input.GetKey(KeyCode.Space) && isGrappling)
        {
            isGrappling = false;
            FPC.useGravity = true;
            LR.enabled = false;
        }
	}

    public void findSpot()
    {
        if(Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, maxDistance, cullingMask))
        {
            isGrappling = true;
            FPC.useGravity = false;
            loc = hit.point;
            LR.enabled = true;
            LR.SetPosition(1, loc);
        }
    }

    public void Flying()
    {
        transform.position = Vector3.Lerp(transform.position, loc, speed * Time.deltaTime / Vector3.Distance(transform.position, loc));
        LR.SetPosition(0, hand.position);

        if (Vector3.Distance(transform.position, loc) < 1.0)
        {
            isGrappling = false;
            FPC.useGravity = true;
            LR.enabled = false;
        }
    }
}
