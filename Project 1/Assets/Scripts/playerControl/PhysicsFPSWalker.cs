﻿using UnityEngine;
using System.Collections;

public class PhysicsFPSWalker : MonoBehaviour
{

    // These variables are for adjusting in the inspector how the object behaves 
    public float maxSpeed = 7;
    public float force = 8;
    public float jumpSpeed = 5;

    // These variables are there for use by the script and don't need to be edited
    private int state = 0;
    public bool grounded = false;
    private float jumpLimit = 0;

    public Vector3 jumpDir;

    Rigidbody myRigidbody;

    // Don't let the Physics Engine rotate this physics object so it doesn't fall over when running
    void Awake()
    {
        myRigidbody = GetComponent<Rigidbody>();
        myRigidbody.freezeRotation = true;
    }

    // This part detects whether or not the object is grounded and stores it in a variable
    void OnCollisionEnter(Collision collision)
    {
        state++;
        if (state > 0)
        {
            grounded = true;
        }
        foreach (ContactPoint contact in collision.contacts)
        {
            print(contact.thisCollider.name + " hit " + contact.otherCollider.name);
            Debug.DrawRay(contact.point, contact.normal, Color.white);
            jumpDir = contact.normal;
            jumpDir = jumpDir + (Vector3.up * 4.0f);
            jumpDir.Normalize();
            Debug.DrawRay(transform.position, jumpDir, Color.red);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            print(contact.thisCollider.name + " hit " + contact.otherCollider.name);
            Debug.DrawRay(contact.point, contact.normal, Color.white);
            jumpDir = contact.normal;
            jumpDir = jumpDir + (Vector3.up * 2.0f);
            jumpDir.Normalize();
            Debug.DrawRay(transform.position, jumpDir, Color.red);
        }
    }

    void OnCollisionExit()
    {
        state--;
        if (state < 1)
        {
            grounded = false;
            state = 0;
        }
    }


    public virtual bool jump
    {
        get
        {
            return Input.GetButton("Jump");
        }
    }

    public virtual float horizontal
    {
        get
        {
            return Input.GetAxis("Horizontal");
        }
    }
    public virtual float vertical
    {
        get
        {
            return Input.GetAxis("Vertical");
        }
    }
    // This is called every physics frame
    void FixedUpdate()
    {
        if (horizontal != 0 || vertical != 0)
        {
            myRigidbody.drag = 0;
        }
        else if (grounded)
        {
            myRigidbody.drag = 10;
        }
        else
        {
            myRigidbody.drag = 0;
        }

        // If the object is grounded and isn't moving at the max speed or higher apply force to move it
        if (grounded)
        {
            myRigidbody.AddForce(transform.rotation * Vector3.forward * vertical * force);
            myRigidbody.AddForce(transform.rotation * Vector3.right * horizontal * force);
        }
        else
        {
            myRigidbody.AddForce(transform.rotation * Vector3.forward * vertical * force / 20);
            myRigidbody.AddForce(transform.rotation * Vector3.right * horizontal * force / 20);
        }

        float yVel = myRigidbody.velocity.y;
        myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, 0, myRigidbody.velocity.z);
        myRigidbody.velocity = Vector3.ClampMagnitude(myRigidbody.velocity, maxSpeed);
        myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, yVel, myRigidbody.velocity.z);

        // This part is for jumping. I only let jump force be applied every 10 physics frames so
        // the player can't somehow get a huge velocity due to multiple jumps in a very short time
        if (jumpLimit < 10) jumpLimit++;

        if (jump && grounded)
        {
            myRigidbody.drag = 0;
            myRigidbody.velocity = new Vector3(myRigidbody.velocity.x, Mathf.Max(0.0f, myRigidbody.velocity.y), myRigidbody.velocity.z);
            myRigidbody.velocity = myRigidbody.velocity + (jumpDir * jumpSpeed);
            jumpLimit = 0;
        }
    }
    ///
}