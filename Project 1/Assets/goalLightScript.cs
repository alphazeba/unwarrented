﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goalLightScript : MonoBehaviour {

    private Light self;
    private float lightScale = 0.2f;
    private float lightScaleRate = 20;
    private float spotScale = 10;
    private float spotScaleRate = 4;

    private float baseLight;
    private float baseSpot;

    private float l, s;

    // Use this for initialization
    void Start () {
        self = this.GetComponent<Light>();

        baseLight = self.intensity;
        baseSpot = self.spotAngle;

        l = 0.0f;
        s = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        float lscalage = Mathf.Sin(l);
        float sscalage = Mathf.Sin(s);

        this.l += lightScaleRate * Time.deltaTime;
        this.s += spotScaleRate * Time.deltaTime;

        l = l % 360;
        s = s % 360;

        self.intensity = baseLight + lscalage * lightScale;
        self.spotAngle = baseSpot + sscalage * spotScale;

	}
}
