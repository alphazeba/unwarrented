﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    public string[] scenes;
    public string[] names;
    bool show = true;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

	// Use this for initialization
	void Start () { 

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            show = true;
        }
	}

    private Rect windowRect = new Rect(20, 20, 120, 50);

    void OnGUI()
    {
        if (show)
        {
            windowRect = GUILayout.Window(0, windowRect, WindowFunction, "Level Select");
        }
    }

    void WindowFunction(int windowID)
    {
        GUILayout.BeginVertical();
        {
            for(int i = 0; i < scenes.Length; i++)
            {
                if (GUILayout.Button(names[i]))
                {
                    show = false;
                    SceneManager.LoadScene(scenes[i]);
                }
            }

            GUILayout.Space(20);

            if (GUILayout.Button("Quit"))
            {
                Application.Quit();
            }
        }
        GUILayout.EndVertical();
    }
}
