﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SceneryPopulator : MonoBehaviour
{

    public bool populate; //"run" or "generate" for example
    public bool delete; //supports multiple buttons

    public GameObject[] items;
    public int count = 0;
    public float area = 1000.0f;
    public float scale = 0.1f;

    void Update()
    {
        if (populate)
            Populate();
        else if (delete)
            Delete();
        populate = false;
        delete = false;
    }

    void Populate()
    {
        for (int i = 0; i < count; i++)
        {
            foreach (GameObject g in items)
            {
                GameObject go = Instantiate(g, this.transform);

                foreach(Collider c in go.GetComponentsInChildren<Collider>())
                {
                    c.enabled = false;
                }

                Vector2 pos = Random.insideUnitCircle;
                go.transform.position = new Vector3(pos.x * area, this.transform.position.y, pos.y * area);

                go.transform.Rotate(0.0f, Random.Range(0.0f, 360.0f), 0.0f);

                go.transform.localScale *= 1 + Random.Range(-scale, scale);
            }
        }
    }

    void Delete()
    {
        foreach (Transform child in transform)
        {
            DestroyImmediate(child.gameObject);
        }
    }
}
