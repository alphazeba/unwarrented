﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonCancerousGrapplingHook : MonoBehaviour {

    private Camera cam;
    public GameObject endpointObj;
    public float maxDistance = 50.0f;
    private LineRenderer lr;
    public Transform hand;
    private Transform lineEnd;
    private Color col;

	private fallDeath faller;

	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        cam = this.GetComponentInChildren<Camera>();
        lr = this.gameObject.GetComponent<LineRenderer>();

        lr.enabled = false;
		faller = this.GetComponent<fallDeath> ();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 point = new Vector3(cam.pixelWidth / 2, cam.pixelHeight / 2, 0);
        Ray ray = cam.ScreenPointToRay(point);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.distance < maxDistance)
            {
                col = Color.black;
                if (Input.GetMouseButtonDown(0))
                {
                    GameObject hitObject = hit.transform.gameObject;
                    GameObject foo = Instantiate(endpointObj, hitObject.transform);
                    foo.transform.position = this.transform.position;

                    lineEnd = foo.transform;

                    SpringJoint j = this.gameObject.AddComponent<SpringJoint>() as SpringJoint;

                    j.connectedBody = foo.GetComponent<Rigidbody>();

                    j.spring = 5;
                    j.damper = 5;

                    StartCoroutine(EndPointDestroyer(foo, j));
                    StartCoroutine(MoveEndPoint(foo, hit));
                }
            }
            else
            {
                col = Color.red;
            }
        }

        if (lineEnd != null)
        {
            lr.SetPosition(0, hand.position);
            lr.SetPosition(1, lineEnd.position);
        }
    }

    void OnGUI()
    {
        GUI.color = col;
        int size = 20;
        float posX = cam.pixelWidth / 2 - size / 2;
        float posY = cam.pixelHeight / 2 - size / 2;
        GUI.Label(new Rect(posX, posY, size, size), "o");
    }

    IEnumerator EndPointDestroyer(GameObject foo, Component c)
    {
		yield return new WaitWhile(() => Input.GetMouseButton(0) && !faller.hasDied());

        lr.enabled = false;
        Destroy(c);
        Destroy(foo);
    }

    IEnumerator MoveEndPoint(GameObject foo, RaycastHit hit)
    {
        yield return new WaitForFixedUpdate();

        foo.transform.position = hit.point;
        lr.enabled = true;
    }
}
